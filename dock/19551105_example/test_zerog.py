import zerogravity.zerog as g

# const
tf_BLACK = (0, 0, 0)
tf_BLUE = (0, 0, 1)
tf_CYAN = (0, 0.5, 0.5)
tf_GREEN = (0, 1, 0)
tf_WHITE = (1, 1, 1)
tf_YELLOW = (0.5, 0.5, 0)
tf_ORANGE = (0.64, 0.32, 0.04)
tf_RED = (1, 0, 0)

# get blender
o_zerog = g.zeroGblender()

# add navigation
o_navi = g.Navigation()
o_zerog.add(o_navi)

# add tourch
o_torch = g.Torch()
o_zerog.add(o_torch)

# add global position system
o_gps = g.GlobalPositionSystem(key="ONE")
o_zerog.add(o_gps)

# get and add mesh
o_cone = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/cone.stl",
    diffuse_color=tf_BLACK,
    visibility_key="NINE"
)
o_zerog.add(o_cone)

o_cube = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/cube.stl",
    diffuse_color=tf_BLUE,
    visibility_key="EIGHT"
)
o_zerog.add(o_cube)

o_cylinder = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/cylinder.stl",
    diffuse_color=tf_CYAN,
    visibility_key="SEVEN"
)
o_zerog.add(o_cylinder)

o_grid = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/grid.stl",
    diffuse_color=tf_GREEN
)
o_zerog.add(o_grid)

o_monkey = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/monkey.stl",
    diffuse_color=tf_WHITE,
    visibility_key="SIX"
)
o_zerog.add(o_monkey)

o_spere = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/sphere.stl",
    diffuse_color=tf_YELLOW,
    visibility_key="FIVE"
)
o_zerog.add(o_spere)

o_torus = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/torus.stl",
    diffuse_color=tf_ORANGE,
    visibility_key="FOUR"
)
o_zerog.add(o_torus)

o_virus = g.Mesh(
    relative_path_file="dock/19551105_example/stl_small/virus.stl",
    diffuse_color=tf_RED,
    visibility_key="THREE"
)
o_zerog.add(o_virus)

# render game
o_zerog.zerog_os = "Windows"
o_zerog.blend_game(b_debug=False)

o_zerog.zerog_os = "Darwin"
o_zerog.blend_game(b_debug=False)

o_zerog.zerog_os = "Linux"
o_zerog.blend_game(b_debug=True)
